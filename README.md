<h1>Vangla</h1>

Vangla is a small and simple reminder. It was designed to be easy to use and minimalistic while staying useful and optimal.<br>To compile and install it, run <code>make</code> and then <code>make install</code>.<br>To check command line options and general use, run vangla --help.<br><br>I've added configuration file support, configuration file is supposed to be in ~/.config/vangla/colours.cfg and you will edit it as, for example:<br>
IMPORTANT = "dark blue";<br>
NORMAL = "green";<br>
MILD = "yellow";

#include "functions.h"
#include "config_vars.h"
#include "config_parser.h"

int main(int argc, char *argv[])
{
	parse();
	if(argc<2)
	{
		printf("Usage: vangla [option] (arguments)");
		return 1;
	}
	else
	{
		if(strcmp(argv[1], "--help")==0)
		{
			printf("Vangla is a reminder program. Its options are:\n-p   Prints all current reminders added.\n-pf [1-3]   Prints all reminders that are of importance specified with the number\n-a \"[Reminder]\" [Importance 1-3]   Adds a reminder, 1-3 is the level of importance.\n-c   Clears the list\n-d [number]   Deletes an entry from the list. Number can be acquired from the -p option.\n\nGeneral use would go as:\nvangla -a \"My reminder\" 1\nvangla -p\nAnd after doing what you need you can run\nvangla -c to clear all reminders(if you've got more of them) or\nvangla -d [number] to delete a reminder at certain position.\n\n");
		}
		else if(strcmp(argv[1], "-p")==0)
		{
			print_items();
		}
		else if(strcmp(argv[1], "-pf")==0)
		{
			switch(strtol(argv[2], NULL, 0))
			{
				case MILD:
					print_items_filtered(MILD);
					break;
				case NORMAL:
					print_items_filtered(NORMAL);
					break;
				case IMPORTANT:
					print_items_filtered(IMPORTANT);
					break;
			}
		}
		else if(strcmp(argv[1], "-c")==0)
		{
			clear_items();
		}
		else if(strcmp(argv[1], "-co")==0)
		{
			count_items();
		}
		else if(strcmp(argv[1], "-d")==0)
		{
			delete_item(strtol(argv[2], NULL, 0));
		}
		else if(strcmp(argv[1], "-a")==0)
		{
			add_item(argv[2], strtol(argv[3], NULL, 0));
		}
	}
	return 0;
}

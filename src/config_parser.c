#include "config_parser.h"

char parse()
{
	FILE* config_file = fopen(CONFIG_PATH, "r");

	if(config_file == NULL)
	{
		perror("Couldn't open the configuration file");
		return 1;
	}

	if(fgetc(config_file) == EOF)
		return 0;

	config_t cfg;
	config_setting_t *setting;
	const char *str;

	config_init(&cfg);

	/* Read the file. If there is an error, report it and exit. */
	if(! config_read_file(&cfg, CONFIG_PATH))
	{
		fprintf(stderr, "%s:%d - %s\n", config_error_file(&cfg),
		config_error_line(&cfg), config_error_text(&cfg));
		config_destroy(&cfg);
		return(EXIT_FAILURE);
	}

	/* Get the store name. */
	if(config_lookup_string(&cfg, "IMPORTANT", &str))
	{
		if(strcmp(str, "green") == 0)
		{
			IMPORTANT_COLOUR = GREEN;
		}
		else if(strcmp(str, "blue") == 0)
		{
			IMPORTANT_COLOUR = BLUE;
		}
		else if(strcmp(str, "red") == 0)
		{
			IMPORTANT_COLOUR = RED;
		}
		else if(strcmp(str, "yellow") == 0)
		{
			IMPORTANT_COLOUR = YELLOW;
		}
		else if(strcmp(str, "light green") == 0)
		{
			IMPORTANT_COLOUR = LIGHT_GREEN;
		}
		else if(strcmp(str, "orange") == 0)
		{
			IMPORTANT_COLOUR = ORANGE;
		}
		else if(strcmp(str, "cyan") == 0)
		{
			IMPORTANT_COLOUR = CYAN;
		}
		else if(strcmp(str, "dark blue") == 0)
		{
			IMPORTANT_COLOUR = DARK_BLUE;
		}
		else if(strcmp(str, "bright green") == 0)
		{
			IMPORTANT_COLOUR = BRIGHT_GREEN;
		}
	}
	if(config_lookup_string(&cfg, "NORMAL", &str))
	{
		if(strcmp(str, "green") == 0)
		{
			NORMAL_COLOUR = GREEN;
		}
		else if(strcmp(str, "blue") == 0)
		{
			NORMAL_COLOUR = BLUE;
		}
		else if(strcmp(str, "red") == 0)
		{
			NORMAL_COLOUR = RED;
		}
		else if(strcmp(str, "yellow") == 0)
		{
			NORMAL_COLOUR = YELLOW;
		}
		else if(strcmp(str, "light green") == 0)
		{
			NORMAL_COLOUR = LIGHT_GREEN;
		}
		else if(strcmp(str, "orange") == 0)
		{
			NORMAL_COLOUR = ORANGE;
		}
		else if(strcmp(str, "cyan") == 0)
		{
			NORMAL_COLOUR = CYAN;
		}
		else if(strcmp(str, "dark blue") == 0)
		{
			NORMAL_COLOUR = DARK_BLUE;
		}
		else if(strcmp(str, "bright green") == 0)
		{
			NORMAL_COLOUR = BRIGHT_GREEN;
		}
	}
	if(config_lookup_string(&cfg, "MILD", &str))
	{
		if(strcmp(str, "green") == 0)
		{
			MILD_COLOUR = GREEN;
		}
		else if(strcmp(str, "blue") == 0)
		{
			MILD_COLOUR = BLUE;
		}
		else if(strcmp(str, "red") == 0)
		{
			MILD_COLOUR = RED;
		}
		else if(strcmp(str, "yellow") == 0)
		{
			MILD_COLOUR = YELLOW;
		}
		else if(strcmp(str, "light green") == 0)
		{
			MILD_COLOUR = LIGHT_GREEN;
		}
		else if(strcmp(str, "orange") == 0)
		{
			MILD_COLOUR = ORANGE;
		}
		else if(strcmp(str, "cyan") == 0)
		{
			MILD_COLOUR = CYAN;
		}
		else if(strcmp(str, "dark blue") == 0)
		{
			MILD_COLOUR = DARK_BLUE;
		}
		else if(strcmp(str, "bright green") == 0)
		{
			MILD_COLOUR = BRIGHT_GREEN;
		}
	}
	/*Those are some colours I chose for the configuration file.
	If you have a theme or colour choice of your terminal which doesn't go well with any of these,
	feel free to add your own colours to config_vars.h and I'll be happy to merge them. Also,
	this is a primitive way of parsing the configuration file so if you've got a better way(which I do have in mind
	but will not modify anything yet), feel free to modify and I will merge it if I find it appropriate
	
	For the list of colour codes, see https://i.stack.imgur.com/KTSQa.png
	*/
	return 0;
}

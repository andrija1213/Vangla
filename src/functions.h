#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "config_vars.h"

#define BUILD_LIST_PATH_(user) "/home/" #user "/.items.list"
#define BUILD_LIST_PATH(user) BUILD_LIST_PATH_(user)
#define BUILD_PATH_TEMP_(user) "/home/" #user "/.items.list.temp"
#define BUILD_PATH_TEMP(user) BUILD_PATH_TEMP_(user)
#define BUILD_CONFIG_PATH_(user) "/home/" #user "/.config/vangla/colours.cfg"
#define BUILD_CONFIG_PATH(user) BUILD_CONFIG_PATH_(user)
#define LIST_PATH BUILD_LIST_PATH(USER)
#define LIST_PATH_TEMP BUILD_PATH_TEMP(USER)
#define CONFIG_PATH BUILD_CONFIG_PATH(USER)

FILE *list;

struct todo_item
{
	char *message[300];
	char importance;
};

void add_item(char *message, char importance);
void print_items();
void print_items_filtered(char importance);
void clear_items();
void count_items();

#endif

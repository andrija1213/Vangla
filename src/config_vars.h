#define IMPORTANT 3
#define NORMAL 2
#define MILD 1

#define RED 196
#define YELLOW 220
#define LIGHT_GREEN 26
#define ORANGE 202
#define CYAN 51
#define DARK_BLUE 17
#define LIGHT_BLUE 31
#define GREEN 34
#define BRIGHT_GREEN 46
#define BLUE 25

extern unsigned char IMPORTANT_COLOUR;
extern unsigned char NORMAL_COLOUR;
extern unsigned char MILD_COLOUR;

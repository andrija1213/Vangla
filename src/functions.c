/*Char is used all over the file for the sake of memory.
Just read it as 1 byte number*/

#include "functions.h"

void add_item(char *message, char importance)
{
	list = fopen(LIST_PATH, "a");
	if(list == NULL)
	{
		perror("Error occured while opening the list file");
		return;
	}
	fprintf(list, "%d %s\n", importance, message);
	fclose(list);
}

void print_items()
{
	list = fopen(LIST_PATH, "r");
	if(list == NULL)
	{
		perror("Error occured while opening the list file");
		return;
	}
	char line [102]; //100 characters + 3 for colour code and space
	char importance;
	char counter = 0;
	while(fgets(line, 102, list) != NULL)
	{
		line[1]='\0';
		importance = strtol(line, NULL, 0);
		switch(importance)
		{
			case MILD:
				printf("%d. \033[38;5;%dmTODO: \033[0m%s", counter++, MILD_COLOUR, line+2);
				break;
			case NORMAL:
				printf("%d. \033[38;5;%dmTODO: \033[0m%s", counter++, NORMAL_COLOUR, line+2);
				break;
			case IMPORTANT:
				printf("%d. \033[38;5;%dmTODO: \033[0m%s", counter++, IMPORTANT_COLOUR, line+2);
				break;
		}
	}
	fclose(list);
}

void print_items_filtered(char filter)
{
	list = fopen(LIST_PATH, "r");
	if(list == NULL)
	{
		perror("Error occured while opening the list file");
		return;
	}
	char line [102]; //100 characters + 3 for colour code and space
	char importance;
	while(fgets(line, 102, list) != NULL)
	{
		line[1]='\0';
		importance = strtol(line, NULL, 0);
		if(importance == filter)
		{
			switch(importance)
			{
				case MILD:
					printf("\033[38;5;%dmTODO: \033[0m%s", MILD_COLOUR, line+2);
					break;
				case NORMAL:
					printf("\033[38;5;%dmTODO: \033[0m%s", NORMAL_COLOUR, line+2);
					break;
				case IMPORTANT:
					printf("\033[38;5;%dmTODO: \033[0m%s", IMPORTANT_COLOUR, line+2);
					break;
			}
		}
	}
	fclose(list);
}

void count_items()
{
	list=fopen(LIST_PATH, "r");
	if(list == NULL)
	{
		perror("Error occured while opening the list file");
		return;
	}
	char counter=0;
	char buf;
	while(!feof(list))
	{
		buf=fgetc(list);
		if(buf == '\n')
		{
			counter++;
		}
	}
	printf("Currently, there are %d items.\n", counter);
}

void clear_items()
{
	list = fopen(LIST_PATH, "w");
	if(list == NULL)
	{
		perror("Error occured while opening the list file");
		return;
	}
	fclose(list);
}

void delete_item(int line_num)
{
	list = fopen(LIST_PATH, "r");
	if(list == NULL)
	{
		perror("Error occured while opening the list file");
		return;
	}
	FILE *list_new = fopen(LIST_PATH_TEMP, "w");
	if(list_new == NULL)
	{
		perror("Error occured while opening the temporary list file");
		return;
	}
	char line[103];
	char counter=0;
	while(fgets(line, 103, list) != NULL)
	{
		if(counter != line_num)
		{
			fprintf(list_new, "%s", line);
			counter++;
		}
		else
		{
			counter++;
		}
	}
	fclose(list);
	fclose(list_new);
	remove(LIST_PATH);
	rename(LIST_PATH_TEMP, LIST_PATH);
}

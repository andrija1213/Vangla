#ifndef PARSER_H
#define PARSER_H

#include <libconfig.h>
#include <string.h>
#include <stdio.h>
#include "functions.h"
#include "config_vars.h"

char parse();

#endif

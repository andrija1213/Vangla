all:vangla

functions.o:src/functions.c src/functions.h src/config_vars.h
	gcc -c src/functions.c -DUSER=$(USER)

vangla.o:src/vangla.c src/config_parser.c src/functions.c
	gcc -c src/vangla.c

config_parser.o:src/config_parser.c src/config_parser.h src/functions.h src/config_vars.h
	gcc -c src/config_parser.c -DUSER=$(USER)

config_vars.o:src/config_vars.c src/config_vars.h
	gcc -c src/config_vars.c

vangla:vangla.o functions.o config_parser.o config_vars.o
	gcc -O2 -s vangla.o functions.o config_parser.o config_vars.o -lconfig -o vangla
	strip vangla

clean:
	rm -rf *.o vangla

install:
	touch ~/.items.list
	sudo ln -s $(PWD)/vangla /usr/bin/vangla
	mkdir ~/.config/vangla
	touch ~/.config/vangla/colours.cfg

uninstall:
	rm ~/.items.list
	sudo rm /usr/bin/vangla
	rm -rf ~/.config/vangla
